---
title: Home
name: Jhosse Millones
date: 2017-02-20T15:26:23-06:00

categories: [one]
tags: [two,three,four]

interests: [
  "Analysis of Data",
  "Big Data",
  "Data Visualization",
  "Machine Learning"
]

applications: [
  "Excel Avanzado",
  "Power BI",
  "SQL Server",
  "SPSS",
  "R Studio"
]

education:
  one:
    course: Statistical Engineering
    university: National University of Engineering
    years: 2019 - In progress
---


I am a data analyst in training. I am currently an intern at the Graduate Unit of the FIEECS UNI, advancing my career with a technical and analytical approach. My skills include advanced use of Excel, Power BI, SQL Server, PostgreSQL, SPSS and R Studio, as well as programming in Python and R. I have a foundation in English as a second language and Spanish as a native language. I am characterized by being a person who adapts quickly to changes as needed, with leadership, initiative, and the ability to work under pressure and face challenges.
