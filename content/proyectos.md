---
title: Projects and Volunteering
name: Jhosse Millones
date: 2023-11-20T00:00:00-05:00  # Adjust this date to the last update date

---

## Volunteering

#### SONRISAS FIEECS | Logistics and Analysis Coordinator (2023)
Responsible for orchestrating the logistics of multiple charitable events aimed at bringing support and joy to underserved communities. Focused on supply chain efficiency and outreach expansion.
- **Achievement:** Improved supply delivery efficiency by 50%, expanded our reach to three new communities, and significantly doubled the positive impact of our events.

#### Karl Pearson Cultural Center | Co-Director of IT and Communications (2023)
Led a comprehensive digital strategy revamp, enhancing the center's visibility and engagement. Oversaw the digital communication channels and branding initiatives.
- **Achievement:** Increased the center's online presence by 75% and improved student engagement by 40%, directly contributing to the successful launch of five new educational programs.

#### FIEECS UNI | Trainer (2019 - 2021)
Played a pivotal role in the transition of faculty members to virtual teaching modalities during the COVID-19 pandemic. Provided training and support for online teaching tools and methodologies.
- **Achievement:** Successfully facilitated the transition for over 100 faculty members, resulting in a 95% increase in faculty satisfaction with the digital tools provided.

## Projects

#### ABET Projects Fair (2023)
Conducted a thorough analysis of student satisfaction with university cafeteria services. Developed and implemented a comprehensive improvement plan based on data insights.
- **Achievement:** The initiative led to a 30% increase in overall service satisfaction, with recommendations enhancing menu quality and service efficiency.